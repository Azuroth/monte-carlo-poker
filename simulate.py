from pypokerengine.api.game import start_poker, setup_config

from callbot import CallBot
from databloggerbot import DataBloggerBot
import numpy as np
from custombot import custombot
from human import human
from learningbot import learningbot

if __name__ == '__main__':
    ai1 = custombot()
    ai2 = learningbot()
    ai3 = DataBloggerBot()
    ai4 = CallBot()
    
    # The stack log contains the stacks of the AIs after each game (the initial stack is 100)
    stack_log1 = []
    stack_log2 = []
    stack_log3 = []
    stack_log4 = []
    
    game_results = []
    
    config = setup_config(max_round = 100 , initial_stack = 100, small_blind_amount = 5)
    config.register_player(name = "CustomBot", algorithm = ai1)
    config.register_player(name = "LearningBot", algorithm = ai2)
    config.register_player(name = "DataBloggerBot", algorithm = ai3)
    config.register_player(name = "CallBot", algorithm = ai4)
    
    for round in range(10):
        game_result = start_poker(config, verbose = 0)

        stack_log1.append([player['stack'] for player in game_result['players'] if player['uuid'] == ai1.uuid])
        stack_log2.append([player['stack'] for player in game_result['players'] if player['uuid'] == ai2.uuid])
        stack_log3.append([player['stack'] for player in game_result['players'] if player['uuid'] == ai3.uuid])
        stack_log4.append([player['stack'] for player in game_result['players'] if player['uuid'] == ai4.uuid])
        
    print('CustomBot: Avg. stack:', '%d' % (int(np.mean(stack_log1))))
    print('LearningBot: Avg. stack:', '%d' % (int(np.mean(stack_log2))))    
    print('DataBloggerBot: Avg. stack:', '%d' % (int(np.mean(stack_log3))))
    print('CallBot: Avg. stack:', '%d' % (int(np.mean(stack_log4))))
    game_results.append([ai1.wins, ai2.wins, ai3.wins, ai4.wins])
    
    stack_log1 = []
    stack_log2 = []

    config = setup_config(max_round = 100 , initial_stack = 100, small_blind_amount = 5)
    config.register_player(name = "CustomBot", algorithm = ai1)
    config.register_player(name = "LearningBot", algorithm = ai2)
    
    for round in range(10):
        game_result = start_poker(config, verbose = 0)

        stack_log1.append([player['stack'] for player in game_result['players'] if player['uuid'] == ai1.uuid])
        stack_log2.append([player['stack'] for player in game_result['players'] if player['uuid'] == ai2.uuid])
        
    print('CustomBot: Avg. stack:', '%d' % (int(np.mean(stack_log1))))
    print('LearningBot: Avg. stack:', '%d' % (int(np.mean(stack_log2))))    
    game_results.append([ai1.wins, ai2.wins, 0, 0])
    
    stack_log2 = []
    stack_log3 = []

    config = setup_config(max_round = 100 , initial_stack = 100, small_blind_amount = 5)
    config.register_player(name = "LearningBot", algorithm = ai2)
    config.register_player(name = "DataBloggerBot", algorithm = ai3)
    
    for round in range(10):
        game_result = start_poker(config, verbose = 0)

        stack_log2.append([player['stack'] for player in game_result['players'] if player['uuid'] == ai2.uuid])
        stack_log3.append([player['stack'] for player in game_result['players'] if player['uuid'] == ai3.uuid])
        
    print('LearningBot: Avg. stack:', '%d' % (int(np.mean(stack_log2))))    
    print('DataBloggerBot: Avg. stack:', '%d' % (int(np.mean(stack_log3))))
    game_results.append([0, ai2.wins, ai3.wins, 0])
    
    stack_log2 = []
    stack_log4 = []

    config = setup_config(max_round = 100 , initial_stack = 100, small_blind_amount = 5)
    config.register_player(name = "LearningBot", algorithm = ai2)
    config.register_player(name = "CallBot", algorithm = ai4)
    
    for round in range(10):
        game_result = start_poker(config, verbose = 0)

        stack_log2.append([player['stack'] for player in game_result['players'] if player['uuid'] == ai2.uuid])
        stack_log4.append([player['stack'] for player in game_result['players'] if player['uuid'] == ai4.uuid])
        
    print('LearningBot: Avg. stack:', '%d' % (int(np.mean(stack_log2))))    
    print('CallBot: Avg. stack:', '%d' % (int(np.mean(stack_log4))))
    game_results.append([0, ai2.wins, 0, ai4.wins])
    
    print(game_results)