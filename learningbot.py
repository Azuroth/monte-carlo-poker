from pypokerengine.engine.hand_evaluator import HandEvaluator
from pypokerengine.players import BasePokerPlayer
from pypokerengine.utils.card_utils import _pick_unused_card, _fill_community_card, gen_cards
import pypokerengine.utils.visualize_utils as U
import pandas as pd
from numpy.random import randint

class learningbot(BasePokerPlayer):
    def __init__(self):
        super().__init__()
        self.wins = 0
        self.losses = 0
        self.last_cards = None
        self.NE = self.create_NE()
        
    def get_index(self, card):
        if card[-1] == 'A': index = '1'
        elif card[-1] == 'K': index = '13'
        elif card[-1] == 'Q': index = '12'
        elif card[-1] == 'J': index = '11'
        elif card[-1] == 'T': index = '10'
        else: index = card[-1]
        return index        
    
    def create_NE(self):
        NE = pd.DataFrame(data = None, columns = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
                       index = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"])
        NE.loc["1"] = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
        NE.loc["2"] = [10, 10, 0.7, 0.7, 0.7, 0.7, 0.8, 0.9, 1.1, 1.4, 2.3, 3.5, 5.8]
        NE.loc["3"] = [10, 0.8, 10, 0.8, 0.9, 0.8, 0.9, 0.9, 1.2, 1.7, 2.5, 3.7, 6.1]
        NE.loc["4"] = [10, 0.9, 1, 10, 1, 1, 1, 1.1, 1.3, 1.9, 2.7, 3.9, 6.5]
        NE.loc["5"] = [10, 1, 1.1, 10, 10, 1.2, 1.3, 1.5, 1.2, 2, 3, 4.4, 7.1]
        NE.loc["6"] = [10, 1, 1.2, 8.1, 10, 10, 5.3, 3.5, 2.6, 2.8, 3.2, 4.8, 7.5]
        NE.loc["7"] = [10, 1, 1.2, 6.9, 10, 10, 10, 7.3, 5.4, 4.5, 4.2, 5.1, 8]
        NE.loc["8"] = [10, 1.2, 1.3, 5, 9.4, 10, 10, 10, 10, 8.7, 6.6, 6.5, 9]
        NE.loc["9"] = [10, 1.8, 2.4, 3.4, 2.2, 10, 10, 10, 10, 10, 10, 10, 10]
        NE.loc["10"] = [10, 3.2, 3.8, 5.2, 5.9, 10, 10, 10, 10, 10, 10, 10, 10]
        NE.loc["11"] = [10, 4.2, 5.2, 5.9, 7.3, 9.3, 10, 10, 10, 10, 10, 10, 10]
        NE.loc["12"] = [10, 6.3, 7.5, 8.6, 10, 10, 10, 10, 10, 10, 10, 10, 10]
        NE.loc["13"] = [10, 9.6, 9.9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
        return NE
    
    def update_NE(self, row, col, n):
        self.NE.loc[row][col] += n
        
    def estimate_win_rate(self, nb_simulation, nb_player, hole_card, community_card = None):
        if not community_card: community_card = []
        
        community_card = gen_cards(community_card)
        hole_card = gen_cards(hole_card)
        win_count = sum([self.calculate_ev(nb_player, hole_card, community_card) for _ in range(nb_simulation)])
        rate = 1.0 * win_count / nb_simulation
        
        if self.last_cards[0][0] == self.last_cards[1][0]:
            row = self.get_index(self.last_cards[1])
            col = self.get_index(self.last_cards[0])
        else:
            row = self.get_index(self.last_cards[0])
            col = self.get_index(self.last_cards[1])
        
        if rate > 0.5:
            self.update_NE(row, col, 1.2)
        else:
            self.update_NE(row, col, -1.2)

        return rate

    def calculate_ev(self, nb_player, hole_card, community_card):
        community_card = _fill_community_card(community_card, used_card = hole_card + community_card)
        unused_cards = _pick_unused_card((nb_player - 1) * 2, hole_card + community_card)
        opponents_hole = [unused_cards[2 * i:2 * i + 2] for i in range(nb_player - 1)]
            
        card_own1 = str(hole_card[0])
        card_own2 = str(hole_card[1])
        card_opp1 = str(opponents_hole[0][0])
        card_opp2 = str(opponents_hole[0][1])
        
        if card_own1[0] == card_own2[0]:
            row_own = self.get_index(card_own2)
            col_own = self.get_index(card_own1)
        else:
            row_own = self.get_index(card_own1)
            col_own = self.get_index(card_own2)
    
        if card_opp1[0] == card_opp2[0]:
            row_opp = self.get_index(card_opp2)
            col_opp = self.get_index(card_opp1)
        else:
            row_opp = self.get_index(card_opp1)
            col_opp = self.get_index(card_opp2)
    
        ev_own = self.NE.loc[row_own][col_own]
        ev_opp = self.NE.loc[row_opp][col_opp]
        
        opponents_score = [HandEvaluator.eval_hand(hole, community_card) for hole in opponents_hole]
        my_score = HandEvaluator.eval_hand(hole_card, community_card)
        
        ev_own = (ev_own + my_score) / 2
        ev_opp = (ev_opp + max(opponents_score)) / 2
        
        if ev_own - ev_opp >= 2:
            return 1
        elif ev_own - ev_opp <= -2:
            return 0
        else:
            return randint(0, 2)   
        
    def declare_action(self, valid_actions, hole_card, round_state):
        self.last_cards = hole_card
        win_rate = self.estimate_win_rate(1000, self.num_players, hole_card, round_state['community_card'])
        can_call = len([item for item in valid_actions if item['action'] == 'call']) > 0
        
        if can_call:
            call_amount = [item for item in valid_actions if item['action'] == 'call'][0]['amount']
        else:
            call_amount = 0

        amount = None

        if win_rate > 0.5:
            raise_amount_options = [item for item in valid_actions if item['action'] == 'raise'][0]['amount']
            if win_rate > 0.85:
                action = 'raise'
                amount = raise_amount_options['max']
            elif win_rate > 0.75:
                action = 'raise'
                amount = raise_amount_options['min']
            else:
                action = 'call'
        else:
            action = 'call' if can_call and call_amount == 0 else 'fold'

        if amount is None:
            items = [item for item in valid_actions if item['action'] == action]
            amount = items[0]['amount']
        
        return action, amount

    def receive_game_start_message(self, game_info):
        self.num_players = game_info['player_num']

    def receive_round_start_message(self, round_count, hole_card, seats):
        print(U.visualize_round_start(round_count, hole_card, seats, self.uuid))

    def receive_street_start_message(self, street, round_state):
        pass
    
    def receive_game_update_message(self, action, round_state):
        pass
    
    def receive_round_result_message(self, winners, hand_info, round_state):
        is_winner = self.uuid in [item['uuid'] for item in winners]
        if int(is_winner):
            self.wins += 1
            
            if self.last_cards[0][0] == self.last_cards[1][0]:
                row = self.get_index(self.last_cards[1])
                col = self.get_index(self.last_cards[0])
            else:
                row = self.get_index(self.last_cards[0])
                col = self.get_index(self.last_cards[1])
                
            self.update_NE(row, col, 2)
        else:
            self.losses += 1
            
            if self.last_cards[0][0] == self.last_cards[1][0]:
                row = self.get_index(self.last_cards[1])
                col = self.get_index(self.last_cards[0])
            else:
                row = self.get_index(self.last_cards[0])
                col = self.get_index(self.last_cards[1])
                
            self.update_NE(row, col, -2)


def setup_ai():
    return learningbot()