from pypokerengine.players import BasePokerPlayer
from pypokerengine.utils.card_utils import _pick_unused_card, _fill_community_card, gen_cards
import pypokerengine.utils.visualize_utils as U
import pandas as pd



class custombot(BasePokerPlayer):
    def __init__(self):
        super().__init__()
        self.wins = 0
        self.losses = 0
        self.last_cards = None
        self.NE = self.create_NE()
        
    def create_NE(self):
        NE = pd.DataFrame(data = None, columns = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
                               index = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"])
        NE.loc["1"] = [20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20]
        NE.loc["2"] = [20, 20, 1.4, 1.4, 1.5, 1.5, 1.6, 1.8, 2.2, 2.9, 4.6, 7, 11.6]
        NE.loc["3"] = [20, 1.7, 20, 1.6, 1.8, 1.7, 1.8, 1.9, 2.5, 3.4, 5, 7.5, 12.2]
        NE.loc["4"] = [20, 1.8, 2, 20, 2.1, 2, 2.1, 2.3, 2.7, 3.8, 5.4, 7.9, 13.1]
        NE.loc["5"] = [20, 2, 2.2, 20, 20, 2.4, 2.6, 3, 3.5, 4.1, 6, 8.9, 14.2]
        NE.loc["6"] = [20, 2, 2.4, 16.3, 20, 20, 10.7, 7, 5.2, 5.7, 6.5, 9.6, 15.1]
        NE.loc["7"] = [20, 2.1, 2.5, 13.9, 20, 20, 20, 14.7, 10.8, 9, 8.5, 10.3, 16.1]
        NE.loc["8"] = [20, 2.5, 2.7, 10.1, 18.8, 20, 20, 20, 20, 17.5, 13.3, 13, 18]
        NE.loc["9"] = [20, 3.7, 4.9, 6.9, 14.4, 20, 20, 20, 20, 20, 20, 20, 20]
        NE.loc["10"] = [20, 6.5, 7.7, 10.5, 11.9, 20, 20, 20, 20, 20, 20, 20, 20]
        NE.loc["11"] = [20, 8.5, 10.6, 13.5, 14.7, 18.6, 20, 20, 20, 20, 20, 20, 20]
        NE.loc["12"] = [20, 12.7, 13.5, 16.3, 20, 20, 20, 20, 20, 20, 20, 20, 20]
        NE.loc["13"] = [20, 19.3, 19.9, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20]
        return NE

    def get_index(self, card):
        if card[-1] == 'A': index = '1'
        elif card[-1] == 'K': index = '13'
        elif card[-1] == 'Q': index = '12'
        elif card[-1] == 'J': index = '11'
        elif card[-1] == 'T': index = '10'
        else: index = card[-1]
        return index
    
    def estimate_win_rate(self, nb_simulation, nb_player, hole_card, community_card = None):
        if not community_card: community_card = []
        
        community_card = gen_cards(community_card)
        hole_card = gen_cards(hole_card)
        win_count = sum([self.calculate_ev(nb_player, hole_card, community_card) for _ in range(nb_simulation)])
        return 1.0 * win_count / nb_simulation
    
    def calculate_ev(self, nb_player, hole_card, community_card):
        community_card = _fill_community_card(community_card, used_card = hole_card + community_card)
        unused_cards = _pick_unused_card((nb_player - 1) * 2, hole_card + community_card)
        opponents_hole = [unused_cards[2 * i:2 * i + 2] for i in range(nb_player - 1)]
            
        card_own1 = str(hole_card[0])
        card_own2 = str(hole_card[1])
        card_opp1 = str(opponents_hole[0][0])
        card_opp2 = str(opponents_hole[0][1])
        
        
        if card_own1[0] == card_own2[0]:
            row_own = self.get_index(card_own2)
            col_own = self.get_index(card_own1)
        else:
            row_own = self.get_index(card_own1)
            col_own = self.get_index(card_own2)
    
        if card_opp1[0] == card_opp2[0]:
            row_opp = self.get_index(card_opp2)
            col_opp = self.get_index(card_opp1)
        else:
            row_opp = self.get_index(card_opp1)
            col_opp = self.get_index(card_opp2)
    
        ev_own = self.NE.loc[row_own][col_own]
        ev_opp = self.NE.loc[row_opp][col_opp]
            
        if ev_own >= ev_opp:
            return 1
        else:
            return 0
    
    def declare_action(self, valid_actions, hole_card, round_state):
        win_rate = self.estimate_win_rate(100, self.num_players, hole_card, round_state['community_card'])
        can_call = len([item for item in valid_actions if item['action'] == 'call']) > 0
        
        if can_call:
            call_amount = [item for item in valid_actions if item['action'] == 'call'][0]['amount']
        else:
            call_amount = 0

        amount = None

        if win_rate > 0.5:
            raise_amount_options = [item for item in valid_actions if item['action'] == 'raise'][0]['amount']
            if win_rate > 0.85:
                action = 'raise'
                amount = raise_amount_options['max']
            elif win_rate > 0.75:
                action = 'raise'
                amount = raise_amount_options['min']
            else:
                action = 'call'
        else:
            action = 'call' if can_call and call_amount == 0 else 'fold'

        if amount is None:
            items = [item for item in valid_actions if item['action'] == action]
            amount = items[0]['amount']
        
        self.last_cards = hole_card
        return action, amount

    def receive_game_start_message(self, game_info):
        self.num_players = game_info['player_num']

    def receive_round_start_message(self, round_count, hole_card, seats):
        print(U.visualize_round_start(round_count, hole_card, seats, self.uuid))

    def receive_street_start_message(self, street, round_state):
        pass
    
    def receive_game_update_message(self, action, round_state):
        pass
    
    def receive_round_result_message(self, winners, hand_info, round_state):
        is_winner = self.uuid in [item['uuid'] for item in winners]
        self.wins += int(is_winner)
        self.losses += int(not is_winner)    

def setup_ai():
    return custombot()